package comh.example.kakas.proyectologisticahumanitaria;


import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.app.AlertDialog;
import android.bluetooth.BluetoothClass;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Array;
import java.util.ArrayList;

import database.Device;
import database.EstadoPersistencia;
import database.Estados;
import database.ProcesosPHP;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, Response.Listener<JSONObject>, Response.ErrorListener{

    TextView edtNombre;
    Button btnGuardar, btnListar, btnLimpiar, btnCerrar;
    Estados savedEstado;
    ProcesosPHP php;
    private int id;

    private final Context context = this;
    private RequestQueue request;
    private JsonObjectRequest jsonObjectRequest;
    private ArrayList<Estados> listaEstados;
    private String serverip ="https://riverajesusgpe98.000webhostapp.com/WebService/";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        request = Volley.newRequestQueue(context);
        initComponents();
        setEvents();
        agregarDatos();
    }

    public void initComponents() {
        this.php = new ProcesosPHP();
        php.setContext(this);
        btnGuardar = findViewById(R.id.btnGuardar);
        btnListar = findViewById(R.id.btnListar);
        btnLimpiar = findViewById(R.id.btnLimpiar);
        edtNombre = findViewById(R.id.txtEstado);
        btnCerrar = findViewById(R.id.btnCerrar);
        savedEstado = null;
    }

    public void setEvents() {
        this.btnGuardar.setOnClickListener(this);
        this.btnListar.setOnClickListener(this);
        this.btnLimpiar.setOnClickListener(this);
        this.btnCerrar.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (isNetworkAvailable()){

            switch (view.getId()) {
                case R.id.btnGuardar:

                    if (edtNombre.getText().toString().equals("")) {
                        edtNombre.setError("Introduce el Estado");
                    }else {
                        EstadoPersistencia source = new EstadoPersistencia(MainActivity.this);
                        source.openDatabase();
                        Estados estados = new Estados();
                        estados.setNombre(edtNombre.getText().toString());
                        if (savedEstado == null) {
                            source.insertContacto(estados);
                            estados.setIdMovil(Device.getSecureId(this));
                            php.insertEstadoWebService(estados);
                            Toast.makeText(MainActivity.this, R.string.mensaje, Toast.LENGTH_SHORT).show();
                            limpiar();
                        } else {
                            source.updateContacto(estados, id);
                            estados.setIdMovil(Device.getSecureId(this));
                            php.actualizarEstadoWebService(estados, id);
                            Toast.makeText(MainActivity.this, R.string.mensajeedit, Toast.LENGTH_SHORT).show();
                            limpiar();
                        }
                        source.close();
                    }
                    break;

                case R.id.btnListar:
                    Intent i = new Intent(MainActivity.this, ListaActivity.class);
                    limpiar();
                    startActivityForResult(i, 0);
                    break;

                case R.id.btnLimpiar:
                    limpiar();
                    break;

                case R.id.btnCerrar:
                    finish();
                    break;
            }
        }else if(view.getId() == R.id.btnListar){
            Intent i = new Intent(MainActivity.this, ListaActivity.class);
            limpiar();
            startActivityForResult(i, 0);

        }else if(view.getId() == R.id.btnLimpiar){
            limpiar();
        }else if(view.getId() == R.id.btnCerrar){
            finish();
        }else{
            Toast.makeText(getApplicationContext(), "Se necesita tener conexión a internet",Toast.LENGTH_SHORT).show();
        }
    }
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        if(Activity.RESULT_OK == resultCode){
            Estados estado = (Estados)data.getSerializableExtra("estados");
            savedEstado = estado;
            id = estado.getId_estado();
            edtNombre.setText(estado.getNombre());
        }else
            limpiar();
    }
    public void limpiar() {
        edtNombre.setText("");
        edtNombre.setError(null);
    }

    public boolean isNetworkAvailable() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo ni = cm.getActiveNetworkInfo();
        return ni != null && ni.isConnected();
    }

    public void agregarDatos() {

        SharedPreferences sharedPreferences = getSharedPreferences("preferencias",MODE_PRIVATE);
        boolean primero = sharedPreferences.getBoolean("primeraVez",true);

        if(primero){

            if(isNetworkAvailable()){


                consultarTodosWebService();

                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putBoolean("primeraVez",false);
                editor.commit();

            }

        }
    }

    public void consultarTodosWebService(){
        try{
            String url = serverip + "wsConsultarTodos.php";
            jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,url,null,this,this);
            request.add(jsonObjectRequest);
        }catch (Exception e) {
            Toast.makeText(getApplicationContext(), "No se han encontrado estados",Toast.LENGTH_LONG).show();
        }
    }
    @Override
    public void onErrorResponse(VolleyError error) {
    }
    @Override
    public void onResponse(JSONObject response) {
        Estados estado = null;
        JSONArray json = response.optJSONArray("estados");
        EstadoPersistencia estadoPersistencia = new EstadoPersistencia(this);
        estadoPersistencia.openDatabase();
        try {
            for(int i=0;i<json.length();i++){
                estado = new Estados();
                JSONObject jsonObject = null;
                jsonObject = json.getJSONObject(i);
                estado.setNombre(jsonObject.optString("nombre"));
                //Toast.makeText(getApplicationContext(), "Estado nombre: " + estado.getNombre(),Toast.LENGTH_SHORT).show();
                estadoPersistencia.insertContacto(estado);
                //Toast.makeText(getApplicationContext(), "No se han encontrado estados",Toast.LENGTH_SHORT).show();
            }
            estadoPersistencia.close();

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

}
